from django.contrib import admin
from .models import Contenido, Comentario, Voto, Pages

# Register your models here.

admin.site.register(Contenido)
admin.site.register(Comentario)
admin.site.register(Voto)
admin.site.register(Pages)
