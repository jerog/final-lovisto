from django.db import models

# Create your models here.

class Pages(models.Model):

    PAGES=[
        ('AE', 'aemet'),
        ('YT', 'youtube'),
        ('WK', 'wikipedia'),
        ('RD', 'reddit'),
        ('NR', 'other'),
    ]

    page = models.CharField(max_length=2, choices=PAGES)
    def __str__(self):
       return str(self.id) + ":" + self.page

class Contenido(models.Model):

    clave = models.CharField(max_length=64)
    usuario = models.TextField()
    valor = models.TextField()
    page = models.ManyToManyField(Pages)
    nvotos_pos = models.IntegerField()
    nvotos_neg = models.IntegerField()
    NumComent = models.IntegerField()
    fecha = models.DateTimeField('published')

    def __str__(self):
       return self.clave+":"+self.valor

class Comentario(models.Model):

    contenido = models.ForeignKey(Contenido,on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200)
    valor = models.TextField()
    usuario = models.TextField()
    fecha = models.DateTimeField('published')
    link_imagen = models.TextField(default="No url")

    def __str__(self):
        return self.titulo + ":" + self.valor

class Voto(models.Model):
    contenido = models.ForeignKey(Contenido,on_delete=models.CASCADE)
    valor = models.TextField()
    usuario = models.TextField()

    def __str__(self):
       return str(self.contenido)+":"+self.usuario