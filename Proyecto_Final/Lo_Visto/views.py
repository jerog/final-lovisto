from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect, render
from . import models
from .models import Contenido, Comentario, Voto
from django.utils import timezone
from django.http import HttpResponse
from bs4 import BeautifulSoup
import requests
import unicodedata
from urllib.request import urlopen
from django.http import JsonResponse


@csrf_exempt
def index(request):

    modo = 0

    if request.method == "POST":
        action = request.POST['action']

        if action == "contenido añadido":

            try:
                contenido = Contenido.objects.get(clave=request.POST['clave'])
                contenido.delete()

            except Contenido.DoesNotExist:
                contenido = Contenido(clave=request.POST['clave'],
                            valor=request.POST['valor'],
                            usuario=request.user.username,
                            nvotos_pos=0,
                            nvotos_neg=0,
                            NumComent=0,
                            fecha=timezone.now())
                contenido.save()

        elif action == "Iniciar sesion":
            return redirect('/login')

        elif action == "Info usuario":
            return redirect('/Lo_Visto/user/' + request.user.username)

        elif action == "Aportaciones":
            return redirect('/Lo_Visto/aportaciones/')

        elif action == "Informacion":
            return redirect('/Lo_Visto/informacion/')

        elif action == "Cerrar sesion":
             logout(request)
             return redirect('/Lo_Visto/')

        elif action == "Modo Claro":
            modo = 0

        elif action == "Modo Oscuro":
            modo = 1

    contenido_list = Contenido.objects.all()
    contenido_list = list(reversed(contenido_list))
    contenido_list_10_publicaciones = contenido_list[0:10]
    contenido_list2 = contenido_list[0:3]
    publicaciones = 10
    iterador = 0

    context = {
        'contenido_list_10_publicaciones': contenido_list_10_publicaciones,
        'contenido_list2': contenido_list2,
        'contenido_list': contenido_list,
        'aportaciones': publicaciones,
        'iterador': iterador,
        'modo': modo
    }

    return render(request, 'Cosas Pagina/paginappal.html', context)

def pagina_usuario(request, llave):

    modo = 0

    if request.method == "POST":
        action=request.POST['action']
        if request.POST['action']== "Pagina principal":
            return redirect('/Lo_Visto')

        elif action== "Cerrar sesion":
            logout(request)
            return redirect('/Lo_Visto')

        elif action== "Aportaciones":
            return redirect('/Lo_Visto/aportaciones/')

        elif action== "Informacion":
            return redirect('/Lo_Visto/informacion/')

        elif action == "Modo Claro":
            modo = 0

        elif action == "Modo Oscuro":
            modo = 1

    contenido_list = Contenido.objects.all()
    comment_list = Comentario.objects.all()
    vote_list = Voto.objects.all()
    contenido_list2 = contenido_list[0:3]

    context = {
         'contenido_list': contenido_list,
         'contenido_list2': contenido_list2,
         'comment_list': comment_list,
         'vote_list': vote_list,
         'modo': modo
     }

    return render(request,'Cosas Pagina/info.html',context)

def pagina_informacion(request):

    modo = 0

    if request.method =="POST":
        action=request.POST['action']

        if request.POST['action']== "Pagina principal":
            return redirect('/Lo_Visto/')

        elif action== "Cerrar sesion":
            logout(request)
            return redirect('/Lo_Visto/')

        elif action== "Iniciar sesion":
            return redirect('/login/')

        elif action== "Info usuario":
            return redirect('/Lo_Visto/user/' + request.user.username)

        elif action== "Informacion":
            return redirect('/Lo_Visto/informacion/')

        elif action== "Aportaciones":
            return redirect('/Lo_Visto/aportaciones/')

        elif action == "Modo Claro":
            modo = 0

        elif action == "Modo Oscuro":
            modo = 1


    contenido_list = Contenido.objects.all()
    contenido_list = list(reversed(contenido_list))
    contenido_list2 = contenido_list[0:3]

    context = {
        'contenido_list': contenido_list,
        'contenido_list2': contenido_list2,
        'modo': modo
    }

    return render(request,'Cosas Pagina/informacion.html',context)

def aportaciones(request):

    modo = 0

    if request.method == "POST":
        action = request.POST['action']

        if request.POST['action'] == "Pagina principal":
            return redirect('/Lo_Visto/')

        elif action == "Cerrar sesion":
            logout(request)

            return redirect('/Lo_Visto/')

        elif action == "Iniciar sesion":
            return redirect('/login/')

        elif action == "Info usuario":
            return redirect('/Lo_Visto/user/' + request.user.username)

        elif action == "Informacion":
            return redirect('/Lo_Visto/informacion/')

        elif action == "Modo Claro":
            modo = 0

        elif action == "Modo Oscuro":
            modo = 1

    contenido_list = list(reversed(Contenido.objects.all()))
    contenido_list2 = contenido_list[0:3]

    context = {
        'contenido_list': contenido_list,
        'contenido_list2': contenido_list2,
        'modo': modo
    }
    return render(request, 'Cosas Pagina/aportaciones.html', context)

@csrf_exempt
def recurso(request,llave):

    modo = 0

    if request.method =="PUT":
        valor = request.body.decode('utf-8')

    if request.method =="POST":

        if request.POST['action'] == "Modif. contenido":

            try:
                contenido = Contenido.objects.get(clave=llave)
                contenido.valor = request.POST['valor']
                contenido.save()

            except Contenido.DoesNotExist:
                contenido=Contenido(clave=llave, valor=request.POST['valor'])
                contenido.save()

        elif request.POST['action'] == "Modo Claro":
            modo = 0

        elif request.POST['action'] == "Modo Oscuro":
            modo = 1

        elif request.POST['action']== "Enviar contenido":
            clave = llave

            try:
                contenido= Contenido.objects.get(clave=clave)
                contenido.delete()

            except Contenido.DoesNotExist:
             pass

            contenido = Contenido(clave=clave, valor=request.POST['valor'],
                          usuario=request.user.username,
                          nvotos_pos=0,
                          nvotos_neg=0,
                          NumComent=0,
                          fecha=timezone.now())
            contenido.save()

        elif request.POST['action'] == "Iniciar sesion":
            return redirect('/login/')

        elif request.POST['action'] == "Cerrar sesion":
            logout(request)
            return redirect('/Lo_Visto/')

        elif request.POST['action'] == "Borrar contenido":
            try:
                contenido = Contenido.objects.get(clave=llave)
                contenido.delete()
                return redirect('/Lo_Visto/')

            except Contenido.DoesNotExist:
                contenido = Contenido.objects.get(clave=llave, valor=valor)
                contenido.save()

        elif request.POST['action'] == "Pagina principal":
            return redirect('/Lo_Visto/')

        elif request.POST['action'] == "Info usuario":
            return redirect('/Lo_Visto/user/<request.user.username>')

        elif request.POST['action'] == "Aportaciones":
            return redirect('/Lo_Visto/aportaciones/')

        elif request.POST['action'] == "Informacion":
            return redirect('/Lo_Visto/informacion/')

        elif request.POST['action']== "Me gusta":

            contenido = Contenido.objects.get(clave=llave)
            contenido.nvotos_pos= contenido.nvotos_pos + 1
            contenido.save()
            votoaux = Voto(contenido=Contenido.objects.get(clave=llave),
                           usuario=request.user.username,
                           valor="Me gusta")
            votoaux.save()

        elif request.POST['action'] == "No me gusta":
            contenido = Contenido.objects.get(clave=llave)
            contenido.nvotos_neg = contenido.nvotos_neg + 1
            contenido.save()
            votoaux = Voto(contenido=Contenido.objects.get(clave=llave),
                           usuario=request.user.username,
                           valor="No me gusta")
            votoaux.save()

        elif request.POST['action']== "borrar voto":
            contenido = Contenido.objects.get(clave=llave)
            votoaux = Voto.objects.get(contenido=Contenido.objects.get(clave=llave),
                                       usuario=request.user.username)

            if votoaux.valor=="Me gusta":
                contenido.nvotos_pos= contenido.nvotos_pos - 1

            elif votoaux.valor=="No me gusta":
                contenido.nvotos_neg= contenido.nvotos_neg - 1
            contenido.save()
            votoaux.delete()

        elif request.POST['action']== "Enviar comentario":

            contenido = Contenido.objects.get(clave=llave)
            contenido.NumComent= contenido.NumComent + 1
            comentario = Comentario(
                contenido=contenido,
                titulo=request.POST['clave'],
                valor=request.POST['valor'],
                fecha=timezone.now(),
                usuario=request.user.username,
                link_imagen=request.POST['link_imagen']
            )

            contenido.save()
            comentario.save()

    try:
        Contenido.objects.get(clave=llave)

    except Contenido.DoesNotExist:

        if request.GET.get("format") == "xml/":

            pagina_xml = loader.get_template('Cosas Pagina/xml.html')
            context = {'Contenido': Contenido.objects.all()}

            return HttpResponse(pagina_xml.render(context, request))

        if request.GET.get("format") == "json/":

            contenido = Contenido.objects.filter().values()

            return JsonResponse({"Contenido": list(contenido)})

        context = {'modo': modo}

        return render(request, 'Cosas Pagina/contenido_no_encontrado.html', context)

    contenido_list = Contenido.objects.all()
    contenido_list2 = contenido_list[0:3]
    comment_list = (Comentario.objects.all())
    vote_list = Voto.objects.all()
    llave=llave

    contenido = Contenido.objects.get(clave=llave)
    url = contenido.valor

    headers = {'User-Agent': 'Mozilla/5.0'}

    try:
        page = requests.get(url, headers=headers)
        soup = BeautifulSoup(page.text, 'html.parser')

        if "es.wikipedia.org" in url:
            try:
                images = soup.findAll('img')
                titulo = soup.find(id="firstHeading").text
                imagen = ((images[0])['src'])
                text = soup.find(id="bodyContent").text
                text2 = text[0:400]

                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'titulo': titulo,
                    'imagen': imagen,
                    'text2': text2,
                    'modo': modo
                }

                return render(request, 'Cosas Pagina/wikipedia.html', context)

            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'modo': modo
                }

            return render(request, 'Cosas Pagina/error_recurso.html', context)

        elif "www.aemet.es" in url:
            try:
                titulo = soup.find("title").text
                temp12 = soup.find("div", class_="no_wrap").text
                fecha12 = soup.find("th", class_="borde_izq_dcha_fecha").text
                maxymin = soup.find("td", class_="alinear_texto_centro no_wrap comunes").text
                precipitacion = soup.find("td", class_="nocomunes").text

                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'titulo': titulo,
                    'temp12': temp12,
                    'fecha12': fecha12,
                    'maxymin': maxymin,
                    'precipitacion': precipitacion,
                    'modo': modo
                }

                return render(request, 'Cosas Pagina/AEMET.html', context)

            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'modo': modo
                }

            return render(request, 'Cosas Pagina/error_recurso.html', context)

        elif "www.reddit.com" in url:

            try:
                images = soup.findAll('img')
                imagen = ((images[0])['src'])
                text = soup.find("p", class_="_1qeIAgB0cPwnLhDF9XSiJM").text
                titulo = soup.find("title").text

                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'text': text,
                    'titulo': titulo,
                    'imagen' : imagen,
                    'modo': modo
                }

                return render(request, 'Cosas Pagina/reddit.html', context)

            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'modo': modo
                }

            return render(request, 'Cosas Pagina/error_recurso.html', context)

        elif "www.elmundotoday.com" in url:

            try:
                images = soup.findAll('img')
                imagen = ((images[1])['src'])
                titulo = soup.find("h1", class_="tdb-title-text").text
                text = soup.find("p").text

                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'text': text,
                    'titulo': titulo,
                    'imagen' : imagen,
                    'modo': modo
                }

                return render(request, 'Cosas Pagina/mundotoday.html', context)

            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'modo': modo
                }

            return render(request, 'Cosas Pagina/error_recurso.html', context)

        elif "elpais.com" in url:

            try:
                images = soup.findAll('img')
                imagen = ((images[1])['src'])
                titulo = soup.find("h1", class_="a_t | font_secondary color_gray_ultra_dark").text
                text = soup.find("p").text

                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'text': text,
                    'titulo': titulo,
                    'imagen' : imagen,
                    'modo': modo
                }

                return render(request, 'Cosas Pagina/elpais.html', context)

            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'modo': modo
                }

            return render(request, 'Cosas Pagina/error_recurso.html', context)

        else:
            try:
                images = soup.findAll('img')
                titulo = soup.find("title").text
                imagen = ((images[0])['src'])

                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'titulo': titulo,
                    'img': img,
                    'modo': modo
                }

                return render(request, 'Cosas Pagina/recurso_no_recon.html', context)

            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comment_list': comment_list,
                    'vote_list': vote_list,
                    'modo': modo
                }

            return render(request, 'Cosas Pagina/error_recurso.html', context)

    except:
        context = {
            'contenido': contenido,
            'comment_list': comment_list,
            'vote_list': vote_list,
            'modo': modo
        }

    return render(request, 'Cosas Pagina/error_recurso.html', context)



